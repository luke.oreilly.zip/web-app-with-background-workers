using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace WebAppWithBackgroundWork
{
    public class BackgroundWorkTracker : IHostedService
    {
        private static readonly object SomethingToLockOn = new object();
        private static readonly List<Task> WorkItems = new List<Task>();
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("BackgroundWorkTracker.StartAsync() called");

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("BackgroundWorkTracker.StopAsync() called");
            
            Console.WriteLine("Waiting for background workers to complete");
            LetWorkItemsComplete();
            
            // Do it again just in case a work item was added during waiting
            LetWorkItemsComplete();
            Console.WriteLine("Finished waiting for background workers to complete");

            return Task.CompletedTask;
        }

        private static void LetWorkItemsComplete()
        {
            Task[] tasksStillRunning;

            lock (SomethingToLockOn)
            {
                tasksStillRunning = WorkItems.ToArray();
            }

            Console.WriteLine($"{tasksStillRunning.Length} work items still running");
            
            //TODO any exception handling needed here?
            Task.WaitAll(tasksStillRunning);
        }

        public static void TrackWorkItem(Task t)
        {
            // Done states
            if (t.Status == TaskStatus.Canceled ||
                t.Status == TaskStatus.Faulted ||
                t.Status == TaskStatus.RanToCompletion)
            {
                return;
            }

            lock (SomethingToLockOn)
            {
                WorkItems.Add(t);
            }

            t.ContinueWith(task =>
            {
                if (!task.IsCompletedSuccessfully)
                {
                    Console.WriteLine("task didn't complete successfully!");
                }

                lock (SomethingToLockOn)
                {
                    WorkItems.Remove(task);
                }
            });
            
            t.Start();
        }
    }
}